<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class simpleController extends Controller
{
    private   $users = [
                         ['name' => 'user1', 'email' => 'user1@domaine.com', 'password' => 'user1pwd'],
                         ['name' => 'user2', 'email' => 'user2@domaine.com', 'password' => 'user2pwd'],
                         ['name' => 'user3', 'email' => 'user3@domaine.com', 'password' => 'user3pwd']
                       ];

    public function accueil() {
        return view('accueil');
    }

    public function liste() {
        return view('users.liste', ['users'=>$this->users]);
    }

    public function add() {
        return view('users.form');
    }

    public function store(Request $request) {
        return view('users.confirmAdd', ['name'=>$request->name, 'email'=>$request->email, 'password'=>$request->password]);
    }
}
