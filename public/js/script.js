axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'

Vue.component('utilisateurs', {
  data : function () {
    return {
      user: {},
      users: {},
    }
  },
  template : `<div>
                <ul v-if="users.length > 0">
                  <li v-for="(utilisateur, index) in users">
                    {{ utilisateur.name }}
                    {{ utilisateur.email }}
                    <button @click="supprimer(utilisateur.id)">supprimer</button>
                  </li>
                </ul>
                <p v-else> Liste est vide. </p>
                <input type="text" v-model="user.name"     placeholder="name"/>
                <input type="text" v-model="user.email"    placeholder="email"/>
                <input type="text" v-model="user.password" placeholder="pwd"/>
                <button class="btn btn-success btn-lg" v-on:click="ajouter">Ajouter !!!</button>
              </div>`,
  mounted () {
    axios.get('/monapplication/restusers')
         .then(response => (this.users = response.data))
  },
  methods: {
    ajouter: function () {
        axios.post('/monapplication/restusers', {'name':this.user.name, 'email':this.user.email, 'password':this.user.password})
             .then(response => ( this.users.push({'name':this.user.name, 'email':this.user.email, 'password':this.user.password}) ) )
    },
    supprimer: function (index) {
      axios.delete('/monapplication/restusers/' + index)
           .then( response => {
              var id = this.users.map(function(e) { return e.id; }).indexOf(index);
              this.users.splice(id,1)
            })
    },
    modifier: function (index, value) {
      //TODO...
    },

  }
});

let app = new Vue({
  el: '#app',
  data: {
    auteur: 'Stéphane ISNARD',
    utilisateurs: [
      'Stéphane',
      'Myriam',
      'Thierry',
      'Jean',
    ],
  },
  methods: {
    initAuteur: function () {
      this.auteur = 'toto'
    },
  }
});
