@extends ('layouts.app')

@section ('titre', 'vueJS app')

@section ('content')
<div id="app">
  <p>
    @{{ auteur }}
    <button @click="initAuteur">init</button>
  </p>
  <utilisateurs></utilisateurs>
</div>
@endsection
