@extends ('layouts.app')

@section ('titre', 'Ajouter un utilisateur')

@section ('content')
<h1>@yield('titre')</h1>
<ul>
    <form method="post" action="/monapplication/users">
      <input type="hidden" name="_token" value="{{ csrf_token() }}" />
      <p>
        <label for="name">Nom: </label><input type="text" name="name" placeholder="Saisir votre nom"/>
      </p>
      <p>
        <label for="email">E-mail: </label><input type="email" name="email" placeholder="Saisir votre email"/>
      </p>
      <p>
        <label for="password">Password: </label><input type="password" name="password" placeholder="Saisir votre mot de passe"/>
      </p>
      <p>
        <input type="submit" value="envoyer" />
      </p>
    </form>
</ul>
@endsection
