@extends ('layouts.app')

@section ('titre', 'Ajouter un utilisateur')

@section ('content')
<h1>@yield('titre')</h1>
<p>
L'utilisateur: {{$user->name}} ({{$user->email}}) a bien été ajouté !
</p>
@endsection
