@extends ('layouts.app')

@section ('titre', 'Supprimer un utilisateur')

@section ('content')
<h1>@yield('titre')</h1>
<p>
L'utilisateur avec l'id: {{$user->id}} ({{$user->name}}) a bien été supprimé !
</p>
@endsection
