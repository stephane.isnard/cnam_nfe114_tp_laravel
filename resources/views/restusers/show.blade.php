@extends ('layouts.app')

@section ('titre', 'Informations utilisateur')

@section ('content')
<h1>@yield('titre')</h1>
<p>
L'utilisateur {{$id}}: {{$user['name']}} ({{$user['email']}})
</p>
@endsection

