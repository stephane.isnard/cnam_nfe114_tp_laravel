@if (isset($user))
<form method="post" action="/monapplication/restusers/{{$id}}">
  <input type="hidden" name="_method" value="put" />
  <input type="hidden" name="id" value="{{ $id }}" />
@else
<form method="post" action="/monapplication/restusers">
@endif
  <input type="hidden" name="_token" value="{{ csrf_token() }}" />
  <p>
    <label for="name">Nom: </label><input type="text" name="name" value="{{$user['name'] or ''}}" placeholder="Saisir votre nom"/>
  </p>
  <p>
    <label for="email">E-mail: </label><input type="email" name="email" value="{{$user['email'] or ''}}" placeholder="Saisir votre email"/>
  </p>
  <p>
    <label for="password">Password: </label><input type="password" name="password" value="{{$user['password'] or ''}}" placeholder="Saisir votre mot de passe"/>
  </p>
  <p>
    <input type="submit" value="envoyer" />
  </p>
</form>
