@extends ('layouts.app')

@section ('titre', 'Ajouter un utilisateur')

@section ('content')
<h1>@yield('titre')</h1>
@include ('restusers.formulaireOnly')
@endsection
