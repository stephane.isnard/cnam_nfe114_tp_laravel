# CNAM NFE114 TP LARAVEL

## Installation

* cloner ce repository
* se positionner dans le répertoire `cnam_nfe114_tp_laravel`
```bash
cd cnam_nfe114_tp_laravel
```
* mette à jour les dépendances à l'aide de composer
```bash
php composer.phar update
```

## Usage avec docker

IMPORTANT: utiliser uniquement si vous connaissez docker. Fourni sans garanti de fonctionnement.

* se positionner dans le répertoire `laradock`
```bash
cd laradock
```
* lancer docker et executer la commande:
```bash
docker-compose up -d apache2 mysql phpmyadmin
```
* apache: http://localhost/monapplication
* mysql: le serveur s'appelle `mysql` (et non localhost)
* phpmyadmin: http://localhost:8080
